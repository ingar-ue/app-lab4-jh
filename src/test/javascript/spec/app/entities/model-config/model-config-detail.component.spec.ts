/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { JobShopTestModule } from '../../../test.module';
import { ModelConfigDetailComponent } from 'app/entities/model-config/model-config-detail.component';
import { ModelConfig } from 'app/shared/model/model-config.model';

describe('Component Tests', () => {
    describe('ModelConfig Management Detail Component', () => {
        let comp: ModelConfigDetailComponent;
        let fixture: ComponentFixture<ModelConfigDetailComponent>;
        const route = ({ data: of({ modelConfig: new ModelConfig(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [JobShopTestModule],
                declarations: [ModelConfigDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ModelConfigDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ModelConfigDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.modelConfig).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
