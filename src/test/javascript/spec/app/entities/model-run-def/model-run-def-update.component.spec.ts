/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { JobShopTestModule } from '../../../test.module';
import { ModelRunDefUpdateComponent } from 'app/entities/model-run-def/model-run-def-update.component';
import { ModelRunDefService } from 'app/entities/model-run-def/model-run-def.service';
import { ModelRunDef } from 'app/shared/model/model-run-def.model';

describe('Component Tests', () => {
    describe('ModelRunDef Management Update Component', () => {
        let comp: ModelRunDefUpdateComponent;
        let fixture: ComponentFixture<ModelRunDefUpdateComponent>;
        let service: ModelRunDefService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [JobShopTestModule],
                declarations: [ModelRunDefUpdateComponent]
            })
                .overrideTemplate(ModelRunDefUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ModelRunDefUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ModelRunDefService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ModelRunDef(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.modelRunDef = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ModelRunDef();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.modelRunDef = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
