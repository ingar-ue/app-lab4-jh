/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { JobShopTestModule } from '../../../test.module';
import { ModelVersionDetailComponent } from 'app/entities/model-version/model-version-detail.component';
import { ModelVersion } from 'app/shared/model/model-version.model';

describe('Component Tests', () => {
    describe('ModelVersion Management Detail Component', () => {
        let comp: ModelVersionDetailComponent;
        let fixture: ComponentFixture<ModelVersionDetailComponent>;
        const route = ({ data: of({ modelVersion: new ModelVersion(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [JobShopTestModule],
                declarations: [ModelVersionDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ModelVersionDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ModelVersionDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.modelVersion).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
