/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { JobShopTestModule } from '../../../test.module';
import { ModelVersionUpdateComponent } from 'app/entities/model-version/model-version-update.component';
import { ModelVersionService } from 'app/entities/model-version/model-version.service';
import { ModelVersion } from 'app/shared/model/model-version.model';

describe('Component Tests', () => {
    describe('ModelVersion Management Update Component', () => {
        let comp: ModelVersionUpdateComponent;
        let fixture: ComponentFixture<ModelVersionUpdateComponent>;
        let service: ModelVersionService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [JobShopTestModule],
                declarations: [ModelVersionUpdateComponent]
            })
                .overrideTemplate(ModelVersionUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ModelVersionUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ModelVersionService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ModelVersion(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.modelVersion = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ModelVersion();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.modelVersion = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
