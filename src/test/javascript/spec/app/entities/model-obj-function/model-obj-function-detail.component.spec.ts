/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { JobShopTestModule } from '../../../test.module';
import { ModelObjFunctionDetailComponent } from 'app/entities/model-obj-function/model-obj-function-detail.component';
import { ModelObjFunction } from 'app/shared/model/model-obj-function.model';

describe('Component Tests', () => {
    describe('ModelObjFunction Management Detail Component', () => {
        let comp: ModelObjFunctionDetailComponent;
        let fixture: ComponentFixture<ModelObjFunctionDetailComponent>;
        const route = ({ data: of({ modelObjFunction: new ModelObjFunction(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [JobShopTestModule],
                declarations: [ModelObjFunctionDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ModelObjFunctionDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ModelObjFunctionDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.modelObjFunction).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
