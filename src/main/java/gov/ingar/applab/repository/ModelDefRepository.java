package gov.ingar.applab.repository;

import gov.ingar.applab.domain.ModelDef;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the ModelDef entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ModelDefRepository extends JpaRepository<ModelDef, Long> {
    Optional<ModelDef> findOneByName(String name);
}
