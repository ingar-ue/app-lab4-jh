package gov.ingar.applab.repository;

import gov.ingar.applab.domain.ModelVersion;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ModelVersion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ModelVersionRepository extends JpaRepository<ModelVersion, Long> {

}
