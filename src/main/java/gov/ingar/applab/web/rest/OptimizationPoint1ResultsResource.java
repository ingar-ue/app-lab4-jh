package gov.ingar.applab.web.rest;

//import com.codahale.metrics.annotation.Timed;
//import gov.ingar.applab.optimization.OptimizationPoint1ResultsService;
//import gov.ingar.applab.optimization.dto.ResultLineChartDTO;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * REST controller for managing OptimizationPoint1Resource.
 */
@RestController
@RequestMapping("/api")
public class OptimizationPoint1ResultsResource {

//    private final OptimizationPoint1ResultsService optimizationPoint1ResultsService;

//    public OptimizationPoint1ResultsResource(OptimizationPoint1ResultsService optimizationPoint1ResultsService) {
//        this.optimizationPoint1ResultsService = optimizationPoint1ResultsService;
//    }
//
//    @GetMapping("/optimization-point-1-results-chart1/{modelDefId}")
//    @Timed
//    public ResponseEntity<ResultLineChartDTO> getOpt1ResultsChart1(@PathVariable Long modelDefId) throws IOException {
//        return new ResponseEntity<>(optimizationPoint1ResultsService.getChart1(modelDefId), HttpStatus.OK);
//    }
//
//    @GetMapping("/optimization-point-1-results-chart2/{modelDefId}")
//    @Timed
//    public ResponseEntity<ResultLineChartDTO> getOpt1ResultsChart2(@PathVariable Long modelDefId) throws IOException {
//        return new ResponseEntity<>(optimizationPoint1ResultsService.getChart2(modelDefId), HttpStatus.OK);
//    }
//
//    @GetMapping("/optimization-point-1-results-chart3/{modelDefId}")
//    @Timed
//    public ResponseEntity<ResultLineChartDTO> getOpt1ResultsChart3(@PathVariable Long modelDefId) throws IOException {
//        return new ResponseEntity<>(optimizationPoint1ResultsService.getChart3(modelDefId), HttpStatus.OK);
//    }

}
