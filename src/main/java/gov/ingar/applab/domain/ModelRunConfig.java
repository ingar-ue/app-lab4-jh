package gov.ingar.applab.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A ModelRunConfig.
 */
@Entity
@Table(name = "model_run_config")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ModelRunConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "param", nullable = false)
    private String param;

    @Column(name = "jhi_value")
    private String value;

    @ManyToOne
    @JsonIgnoreProperties("")
    private ModelRunDef modelRunDef;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParam() {
        return param;
    }

    public ModelRunConfig param(String param) {
        this.param = param;
        return this;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getValue() {
        return value;
    }

    public ModelRunConfig value(String value) {
        this.value = value;
        return this;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ModelRunDef getModelRunDef() {
        return modelRunDef;
    }

    public ModelRunConfig modelRunDef(ModelRunDef modelRunDef) {
        this.modelRunDef = modelRunDef;
        return this;
    }

    public void setModelRunDef(ModelRunDef modelRunDef) {
        this.modelRunDef = modelRunDef;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ModelRunConfig modelRunConfig = (ModelRunConfig) o;
        if (modelRunConfig.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), modelRunConfig.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ModelRunConfig{" +
            "id=" + getId() +
            ", param='" + getParam() + "'" +
            ", value='" + getValue() + "'" +
            "}";
    }
}
