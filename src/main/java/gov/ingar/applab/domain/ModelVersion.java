package gov.ingar.applab.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A ModelVersion.
 */
@Entity
@Table(name = "model_version")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ModelVersion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "version", nullable = false)
    private Integer version;

    @Column(name = "creation_date")
    private LocalDate creationDate;

    @Column(name = "src_path")
    private String srcPath;

    @Lob
    @Column(name = "src_content")
    private String srcContent;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JsonIgnoreProperties("")
    private ModelDef modelDef;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public ModelVersion version(Integer version) {
        this.version = version;
        return this;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public ModelVersion creationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public String getSrcPath() {
        return srcPath;
    }

    public ModelVersion srcPath(String srcPath) {
        this.srcPath = srcPath;
        return this;
    }

    public void setSrcPath(String srcPath) {
        this.srcPath = srcPath;
    }

    public String getSrcContent() {
        return srcContent;
    }

    public ModelVersion srcContent(String srcContent) {
        this.srcContent = srcContent;
        return this;
    }

    public void setSrcContent(String srcContent) {
        this.srcContent = srcContent;
    }

    public String getDescription() {
        return description;
    }

    public ModelVersion description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ModelDef getModelDef() {
        return modelDef;
    }

    public ModelVersion modelDef(ModelDef modelDef) {
        this.modelDef = modelDef;
        return this;
    }

    public void setModelDef(ModelDef modelDef) {
        this.modelDef = modelDef;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ModelVersion modelVersion = (ModelVersion) o;
        if (modelVersion.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), modelVersion.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ModelVersion{" +
            "id=" + getId() +
            ", version=" + getVersion() +
            ", creationDate='" + getCreationDate() + "'" +
            ", srcPath='" + getSrcPath() + "'" +
            ", srcContent='" + getSrcContent() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
