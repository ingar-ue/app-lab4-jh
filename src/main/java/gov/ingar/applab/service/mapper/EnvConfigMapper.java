package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.EnvConfigDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity EnvConfig and its DTO EnvConfigDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EnvConfigMapper extends EntityMapper<EnvConfigDTO, EnvConfig> {



    default EnvConfig fromId(Long id) {
        if (id == null) {
            return null;
        }
        EnvConfig envConfig = new EnvConfig();
        envConfig.setId(id);
        return envConfig;
    }
}
