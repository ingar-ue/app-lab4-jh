package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.MachineDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Machine and its DTO MachineDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MachineMapper extends EntityMapper<MachineDTO, Machine> {



    default Machine fromId(Long id) {
        if (id == null) {
            return null;
        }
        Machine machine = new Machine();
        machine.setId(id);
        return machine;
    }
}
