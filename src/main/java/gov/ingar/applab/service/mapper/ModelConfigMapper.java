package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.ModelConfigDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ModelConfig and its DTO ModelConfigDTO.
 */
@Mapper(componentModel = "spring", uses = {ModelDefMapper.class})
public interface ModelConfigMapper extends EntityMapper<ModelConfigDTO, ModelConfig> {

    @Mapping(source = "modelDef.id", target = "modelDefId")
    ModelConfigDTO toDto(ModelConfig modelConfig);

    @Mapping(source = "modelDefId", target = "modelDef")
    ModelConfig toEntity(ModelConfigDTO modelConfigDTO);

    default ModelConfig fromId(Long id) {
        if (id == null) {
            return null;
        }
        ModelConfig modelConfig = new ModelConfig();
        modelConfig.setId(id);
        return modelConfig;
    }
}
