package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.TaskDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Task and its DTO TaskDTO.
 */
@Mapper(componentModel = "spring", uses = {JobMapper.class, MachineMapper.class})
public interface TaskMapper extends EntityMapper<TaskDTO, Task> {

    @Mapping(source = "job.id", target = "jobId")
    @Mapping(source = "machine.id", target = "machineId")
    TaskDTO toDto(Task task);

    @Mapping(source = "jobId", target = "job")
    @Mapping(source = "machineId", target = "machine")
    Task toEntity(TaskDTO taskDTO);

    default Task fromId(Long id) {
        if (id == null) {
            return null;
        }
        Task task = new Task();
        task.setId(id);
        return task;
    }
}
