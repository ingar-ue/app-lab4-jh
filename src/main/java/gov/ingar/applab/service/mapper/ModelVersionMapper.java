package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.ModelVersionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ModelVersion and its DTO ModelVersionDTO.
 */
@Mapper(componentModel = "spring", uses = {ModelDefMapper.class})
public interface ModelVersionMapper extends EntityMapper<ModelVersionDTO, ModelVersion> {

    @Mapping(source = "modelDef.id", target = "modelDefId")
    ModelVersionDTO toDto(ModelVersion modelVersion);

    @Mapping(source = "modelDefId", target = "modelDef")
    ModelVersion toEntity(ModelVersionDTO modelVersionDTO);

    default ModelVersion fromId(Long id) {
        if (id == null) {
            return null;
        }
        ModelVersion modelVersion = new ModelVersion();
        modelVersion.setId(id);
        return modelVersion;
    }
}
