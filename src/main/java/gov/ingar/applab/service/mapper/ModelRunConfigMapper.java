package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.ModelRunConfigDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ModelRunConfig and its DTO ModelRunConfigDTO.
 */
@Mapper(componentModel = "spring", uses = {ModelRunDefMapper.class})
public interface ModelRunConfigMapper extends EntityMapper<ModelRunConfigDTO, ModelRunConfig> {

    @Mapping(source = "modelRunDef.id", target = "modelRunDefId")
    ModelRunConfigDTO toDto(ModelRunConfig modelRunConfig);

    @Mapping(source = "modelRunDefId", target = "modelRunDef")
    ModelRunConfig toEntity(ModelRunConfigDTO modelRunConfigDTO);

    default ModelRunConfig fromId(Long id) {
        if (id == null) {
            return null;
        }
        ModelRunConfig modelRunConfig = new ModelRunConfig();
        modelRunConfig.setId(id);
        return modelRunConfig;
    }
}
