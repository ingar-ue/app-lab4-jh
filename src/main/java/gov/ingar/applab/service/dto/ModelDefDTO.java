package gov.ingar.applab.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import gov.ingar.applab.domain.enumeration.Language;
import gov.ingar.applab.domain.enumeration.Solver;

/**
 * A DTO for the ModelDef entity.
 */
public class ModelDefDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Language language;

    @NotNull
    private Solver solver;

    private Integer currentVersion;

    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Solver getSolver() {
        return solver;
    }

    public void setSolver(Solver solver) {
        this.solver = solver;
    }

    public Integer getCurrentVersion() {
        return currentVersion;
    }

    public void setCurrentVersion(Integer currentVersion) {
        this.currentVersion = currentVersion;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ModelDefDTO modelDefDTO = (ModelDefDTO) o;
        if (modelDefDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), modelDefDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ModelDefDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", language='" + getLanguage() + "'" +
            ", solver='" + getSolver() + "'" +
            ", currentVersion=" + getCurrentVersion() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
