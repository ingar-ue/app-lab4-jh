package gov.ingar.applab.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the ModelVersion entity.
 */
public class ModelVersionDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer version;

    private LocalDate creationDate;

    private String srcPath;

    @Lob
    private String srcContent;

    private String description;

    private Long modelDefId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public String getSrcPath() {
        return srcPath;
    }

    public void setSrcPath(String srcPath) {
        this.srcPath = srcPath;
    }

    public String getSrcContent() {
        return srcContent;
    }

    public void setSrcContent(String srcContent) {
        this.srcContent = srcContent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getModelDefId() {
        return modelDefId;
    }

    public void setModelDefId(Long modelDefId) {
        this.modelDefId = modelDefId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ModelVersionDTO modelVersionDTO = (ModelVersionDTO) o;
        if (modelVersionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), modelVersionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ModelVersionDTO{" +
            "id=" + getId() +
            ", version=" + getVersion() +
            ", creationDate='" + getCreationDate() + "'" +
            ", srcPath='" + getSrcPath() + "'" +
            ", srcContent='" + getSrcContent() + "'" +
            ", description='" + getDescription() + "'" +
            ", modelDef=" + getModelDefId() +
            "}";
    }
}
