package gov.ingar.applab.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ModelRunConfig entity.
 */
public class ModelRunConfigDTO implements Serializable {

    private Long id;

    @NotNull
    private String param;

    private String value;

    private Long modelRunDefId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getModelRunDefId() {
        return modelRunDefId;
    }

    public void setModelRunDefId(Long modelRunDefId) {
        this.modelRunDefId = modelRunDefId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ModelRunConfigDTO modelRunConfigDTO = (ModelRunConfigDTO) o;
        if (modelRunConfigDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), modelRunConfigDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ModelRunConfigDTO{" +
            "id=" + getId() +
            ", param='" + getParam() + "'" +
            ", value='" + getValue() + "'" +
            ", modelRunDef=" + getModelRunDefId() +
            "}";
    }
}
