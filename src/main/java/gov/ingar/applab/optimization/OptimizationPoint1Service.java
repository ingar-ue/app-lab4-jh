package gov.ingar.applab.optimization;

import gov.ingar.applab.domain.ModelRunDef;
import gov.ingar.applab.optimization.dto.OptimizationPoint1DTO;
import gov.ingar.applab.repository.*;
import gov.ingar.applab.security.SecurityUtils;
import gov.ingar.applab.service.ModelConfigService;
import gov.ingar.applab.service.ModelDefService;
import gov.ingar.applab.service.dto.*;
import gov.ingar.applab.service.mapper.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class OptimizationPoint1Service {

    private final Logger log = LoggerFactory.getLogger(OptimizationPoint1Service.class);

    private final ModelDefService modelDefService;
    private final ModelConfigService modelConfigService;

    private final JobRepository jobRepository;
    private final TaskRepository taskRepository;
    private final MachineRepository machineRepository;

    private final JobMapper jobMapper;
    private final TaskMapper taskMapper;
    private final MachineMapper machineMapper;


    public OptimizationPoint1Service(JobRepository jobRepository, TaskRepository taskRepository, MachineRepository machineRepository,
                                    JobMapper jobMapper, TaskMapper taskMapper, MachineMapper machineMapper,
                                    ModelDefService modelDefService, ModelConfigService modelConfigService) {
        this.modelDefService = modelDefService;
        this.modelConfigService = modelConfigService;

        this.jobRepository = jobRepository;
        this.taskRepository = taskRepository;
        this.machineRepository = machineRepository;

        this.jobMapper = jobMapper;
        this.taskMapper = taskMapper;
        this.machineMapper = machineMapper;
    }

    public static String getCurrentUsername() {
        Optional<String> cul = SecurityUtils.getCurrentUserLogin();
        return cul.isPresent() ? cul.get() : "test";
    }

    public OptimizationPoint1DTO loadCase() {
        ModelRunDef runDef = new ModelRunDef();
        Optional<ModelDefDTO> optDef = modelDefService.findOneByName("CP-SAT SCHEDULING");
        ModelDefDTO def = optDef.get();

        OptimizationPoint1DTO op1 = loadCaseInput();

        runDef.setName(def.getName());
        runDef.setLanguage(def.getLanguage());
        runDef.setSolver(def.getSolver());
        runDef.setVersion(def.getCurrentVersion());
        runDef.setProgress(0);
        runDef.setUser(this.getCurrentUsername());

        op1.setModelRunDef(runDef);
        op1.setModelDefId(def.getId());

        return op1;
    }

    private OptimizationPoint1DTO loadCaseInput() {
        OptimizationPoint1DTO dto = new OptimizationPoint1DTO();

        dto.setMachines(this.machineList());
        dto.setJobs(this.jobList());
        dto.setTasks(this.taskList());

        return dto;
    }

    private List<MachineDTO> machineList() {
        return machineMapper.toDto(machineRepository.findAll());
    }

    private List<JobDTO> jobList() {
        return jobMapper.toDto(jobRepository.findAll());
    }

    private List<TaskDTO> taskList() {
        return taskMapper.toDto(taskRepository.findAll());
    }

}
