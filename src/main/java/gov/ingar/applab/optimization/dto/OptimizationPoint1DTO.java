package gov.ingar.applab.optimization.dto;

import gov.ingar.applab.domain.ModelRunDef;
import gov.ingar.applab.service.dto.*;
import java.io.Serializable;
import java.util.List;

public class OptimizationPoint1DTO implements Serializable {
    private List<MachineDTO> machines;
    private List<JobDTO> jobs;
    private List<TaskDTO> tasks;
    private ModelRunDef modelRunDef;
    private Long modelDefId;

    public List<MachineDTO> getMachines() {
        return machines;
    }

    public void setMachines(List<MachineDTO> machines) {
        this.machines = machines;
    }

    public List<JobDTO> getJobs() {
        return jobs;
    }

    public void setJobs(List<JobDTO> jobs) {
        this.jobs = jobs;
    }

    public List<TaskDTO> getTasks() {
        return tasks;
    }

    public void setTasks(List<TaskDTO> tasks) {
        this.tasks = tasks;
    }

    public ModelRunDef getModelRunDef() {
        return modelRunDef;
    }

    public void setModelRunDef(ModelRunDef modelRunDef) {
        this.modelRunDef = modelRunDef;
    }

    public Long getModelDefId() {
        return modelDefId;
    }

    public void setModelDefId(Long modelDefId) {
        this.modelDefId = modelDefId;
    }
}
