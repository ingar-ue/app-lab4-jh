import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JobShopSharedModule } from 'app/shared';
import { AboutComponent, aboutRoute } from './';

const ENTITY_STATES = [...aboutRoute];

@NgModule({
    imports: [JobShopSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [AboutComponent],
    entryComponents: [AboutComponent],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JobShopAboutModule {}
