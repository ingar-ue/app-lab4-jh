import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { JobShopAboutModule } from './about/about.module';

@NgModule({
    imports: [JobShopAboutModule],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JobShopOptModule {}
