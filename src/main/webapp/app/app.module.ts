import './vendor.ts';

import { NgModule, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { Ng2Webstorage, LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { JhiEventManager } from 'ng-jhipster';

import { AuthInterceptor } from './blocks/interceptor/auth.interceptor';
import { AuthExpiredInterceptor } from './blocks/interceptor/auth-expired.interceptor';
import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { JobShopSharedModule } from 'app/shared';
// import { JobShopOptModule } from 'app/opt';
import { JobShopCoreModule } from 'app/core';
import { JobShopAppRoutingModule } from './app-routing.module';
import { JobShopHomeModule } from './home/home.module';
import { JobShopAccountModule } from './account/account.module';
import { JobShopEntityModule } from './entities/entity.module';
import { JobShopOptModule } from './opt/opt.module';
import * as moment from 'moment';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { JhiMainComponent, NavbarComponent, FooterComponent, PageRibbonComponent, ActiveMenuDirective, ErrorComponent } from './layouts';
import { StompRService } from '@stomp/ng2-stompjs';
import { AgGridModule } from 'ag-grid-angular';
import { ChartModule } from 'angular2-chartjs';
import { SuiModule } from 'ng2-semantic-ui';

@NgModule({
    imports: [
        BrowserModule,
        JobShopAppRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-' }),
        JobShopSharedModule,
        // JobShopOptModule,
        JobShopCoreModule,
        JobShopHomeModule,
        JobShopAccountModule,
        JobShopEntityModule,
        JobShopOptModule,
        // jhipster-needle-angular-add-module JHipster will add new module here
        AgGridModule.withComponents([]),
        ChartModule,
        SuiModule
    ],
    declarations: [JhiMainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true,
            deps: [LocalStorageService, SessionStorageService]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthExpiredInterceptor,
            multi: true,
            deps: [Injector]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorHandlerInterceptor,
            multi: true,
            deps: [JhiEventManager]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: NotificationInterceptor,
            multi: true,
            deps: [Injector]
        },
        StompRService
    ],
    bootstrap: [JhiMainComponent]
})
export class JobShopAppModule {
    constructor(private dpConfig: NgbDatepickerConfig) {
        this.dpConfig.minDate = { year: moment().year() - 100, month: 1, day: 1 };
    }
}
