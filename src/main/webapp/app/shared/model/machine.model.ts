export interface IMachine {
    id?: number;
    serialNumber?: string;
    model?: string;
}

export class Machine implements IMachine {
    constructor(public id?: number, public serialNumber?: string, public model?: string) {}
}
