export interface ITask {
    id?: number;
    name?: string;
    description?: string;
    processingTime?: number;
    order?: number;
    jobId?: number;
    machineId?: number;
}

export class Task implements ITask {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public processingTime?: number,
        public order?: number,
        public jobId?: number,
        public machineId?: number
    ) {}
}
