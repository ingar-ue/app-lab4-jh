import { IModelRunDef, ModelRunDef } from 'app/shared/model/model-run-def.model';
import { IMachine, Machine } from 'app/shared/model/machine.model';
import { IJob, Job } from 'app/shared/model/job.model';
import { ITask, Task } from 'app/shared/model/task.model';

export interface IOptimizationPoint1 {
    modelRunDef?: IModelRunDef;
    modelDefId?: number;
    machines?: IMachine;
    jobs?: IJob;
    tasks?: ITask;
}

export class OptimizationPoint1 implements IOptimizationPoint1 {
    constructor(
        public modelRunDef?: ModelRunDef,
        public modelDefId?: number,
        public machines?: Machine,
        public jobs?: Job,
        public tasks?: Task
    ) {}
}
