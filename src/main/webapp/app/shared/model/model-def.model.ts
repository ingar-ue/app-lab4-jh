export const enum Language {
    GAMS = 'GAMS',
    PYOMO = 'PYOMO',
    ORTOOLS = 'ORTOOLS'
}

export const enum Solver {
    CPLEX = 'CPLEX',
    GUROBI = 'GUROBI',
    GLPK = 'GLPK',
    CBC = 'CBC',
    CPSAT = 'CPSAT',
    GLOP = 'GLOP'
}

export interface IModelDef {
    id?: number;
    name?: string;
    language?: Language;
    solver?: Solver;
    currentVersion?: number;
    description?: string;
}

export class ModelDef implements IModelDef {
    constructor(
        public id?: number,
        public name?: string,
        public language?: Language,
        public solver?: Solver,
        public currentVersion?: number,
        public description?: string
    ) {}
}
