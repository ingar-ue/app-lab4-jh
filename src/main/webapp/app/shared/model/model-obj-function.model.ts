export interface IModelObjFunction {
    id?: number;
    code?: string;
    name?: string;
    description?: string;
    modelDefId?: number;
}

export class ModelObjFunction implements IModelObjFunction {
    constructor(public id?: number, public code?: string, public name?: string, public description?: string, public modelDefId?: number) {}
}
