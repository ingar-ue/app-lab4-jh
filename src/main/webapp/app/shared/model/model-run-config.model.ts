export interface IModelRunConfig {
    id?: number;
    param?: string;
    value?: string;
    modelRunDefId?: number;
}

export class ModelRunConfig implements IModelRunConfig {
    constructor(public id?: number, public param?: string, public value?: string, public modelRunDefId?: number) {}
}
