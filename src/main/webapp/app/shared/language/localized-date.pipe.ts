import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import { JhiLanguageService } from 'ng-jhipster';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';
@Pipe({
    name: 'localizedDate'
})
export class LocalizedDatePipe implements PipeTransform {
    lang = 'en';
    constructor(private languageService: JhiLanguageService) {
        this.getlang();
    }
    getlang() {
        this.languageService.getCurrent().then(
            function(langKeyValue) {
                registerLocaleData(localeEs, 'es');
                switch (langKeyValue) {
                    case 'en': {
                        this.lang = 'en';
                        break;
                    }
                    case 'es': {
                        this.lang = 'es';
                        break;
                    }
                }
            }.bind(this)
        );
    }
    transform(value: any, pattern = 'mediumDate'): any {
        const datePipe: DatePipe = new DatePipe(this.lang);
        return datePipe.transform(value, pattern);
    }
}
