import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'customFilter'
})
@Injectable()
export class CustomFilterPipe implements PipeTransform {
    transform(items: any[], field: string, value: string) {
        if (items && items.length && field && value) {
            return items.filter(item => {
                if (
                    item[field] &&
                    String(item[field])
                        .toLowerCase()
                        .indexOf(value.toLowerCase()) !== -1
                ) {
                    return true;
                }
                return false;
            });
        } else {
            return items;
        }
    }
}
