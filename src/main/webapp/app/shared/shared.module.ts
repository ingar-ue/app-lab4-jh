import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

import { NgbDateMomentAdapter } from './util/datepicker-adapter';
import { JobShopSharedLibsModule, JobShopSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

import { StringFilterPipe } from 'app/shared/pipes/string-filter.pipe';
import { CustomFilterPipe } from 'app/shared/pipes/custom-filter.pipe';
import { SortPipe } from 'app/shared/pipes/sort.pipe';
import { GroupByPipe } from 'app/shared/pipes/group-by.pipe';
import { OptimizationPoint1ChartsComponent } from 'app/shared/chart/optimization-point-1-charts.component';
import { ChartModule } from 'angular2-chartjs';
import { SuiModule } from 'ng2-semantic-ui';

import { LocalizedDatePipe } from 'app/shared/language/localized-date.pipe';

@NgModule({
    imports: [JobShopSharedLibsModule, JobShopSharedCommonModule, ChartModule, SuiModule],
    declarations: [
        JhiLoginModalComponent,
        HasAnyAuthorityDirective,
        StringFilterPipe,
        CustomFilterPipe,
        SortPipe,
        GroupByPipe,
        LocalizedDatePipe,
        OptimizationPoint1ChartsComponent
    ],
    providers: [{ provide: NgbDateAdapter, useClass: NgbDateMomentAdapter }, OptimizationPoint1ChartsComponent, SortPipe],
    entryComponents: [JhiLoginModalComponent, OptimizationPoint1ChartsComponent],
    exports: [
        JobShopSharedCommonModule,
        JhiLoginModalComponent,
        HasAnyAuthorityDirective,
        StringFilterPipe,
        CustomFilterPipe,
        SortPipe,
        GroupByPipe,
        OptimizationPoint1ChartsComponent,
        LocalizedDatePipe
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JobShopSharedModule {}
