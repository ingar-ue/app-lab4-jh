import { Component, Input, OnChanges } from '@angular/core';
import { OptimizationPoint1Service } from 'app/entities/optimization-point-1/optimization-point-1.service';
import { IModelRunDef } from 'app/shared/model/model-run-def.model';
import { ChartService } from 'app/shared/chart/chart.service';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { Chart } from 'chart.js';

@Component({
    selector: 'jhi-optimization-point-1-charts',
    templateUrl: './optimization-point-1-charts.component.html'
})
export class OptimizationPoint1ChartsComponent implements OnChanges {
    charts: any;
    chartLabels: any;
    json = JSON;

    @Input() currentChart: number;
    @Input() change: number;
    @Input() showSidebar: boolean;
    @Input() chartColumnSize: number;
    @Input() modelRunDef: IModelRunDef;

    constructor(
        private optimizationPoint1Service: OptimizationPoint1Service,
        private chartService: ChartService,
        private translateService: TranslateService
    ) {
        this.chartInit();
        this.showSidebar = true;
        this.chartColumnSize = 8;

        Chart.Legend.prototype.afterFit = function() {
            this.height = this.height + 30;
        };

        this.setLanguageValues();
        this.translateService.onLangChange.subscribe((event: LangChangeEvent) => {
            this.setLanguageValues();
            this.setData('chart1', this.modelRunDef);
        });
    }

    setLanguageValues() {
        this.chartLabels = {
            time: this.translateService.instant('jobShopApp.optimization.op1.time'),
            machines: this.translateService.instant('jobShopApp.optimization.op1.machines')
        };
    }

    ngOnChanges() {
        if (this.modelRunDef.status === 'FINISHED') {
            this.setData('chart1', this.modelRunDef);
        }
    }

    chartInit() {
        this.charts = {};
        this.currentChart = 1;
        if (this.modelRunDef && this.modelRunDef.status === 'FINISHED') {
            this.setData('chart1', this.modelRunDef);
        }
    }

    setData(chart, data) {
        this.charts[chart] = this.chartService.setChartConfig(data, this.chartLabels.time, this.chartLabels.machines);
    }

    getTotalTime() {
        return JSON.parse(atob(this.modelRunDef.userResult)).time;
    }
}
