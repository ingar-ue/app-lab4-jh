import { Injectable } from '@angular/core';
import { SortPipe } from 'app/shared/pipes/sort.pipe';

@Injectable({ providedIn: 'root' })
export class ChartService {
    jobColor: object;
    availableColors: any;

    constructor(public sort: SortPipe) {}

    setChartConfig(modelRunDef, labelX, labelY) {
        this.jobColor = {};
        this.availableColors = [
            'rgba(0, 166, 90, .8)',
            'rgba(0, 192, 239, .8)',
            'rgba(57, 204, 204, .8)',
            'rgba(75, 148, 192, .8)',
            'rgba(243, 156, 18, .8)',
            'rgba(166, 175, 24, .8)',
            'rgba(210, 214, 222, .8)',
            'rgba(147, 42, 182, .8)'
        ];

        const res = JSON.parse(atob(modelRunDef.userResult));
        const dataSetInput = JSON.parse(atob(modelRunDef.otherResult));

        // armar datos
        const chartData = { labels: [], datasets: [] };
        let machineCount = res.solution.length;

        for (const machine of res.solution) {
            for (const task of machine) {
                const pointsData = [];
                for (let i = task.start; i <= task.start + task.duration; i++) {
                    pointsData.push({ x: i, y: machineCount });
                }

                chartData.labels = this.getJobNames(dataSetInput.model.jobs);
                chartData.datasets.push({
                    fill: false,
                    borderWidth: 40,
                    pointRadius: 0,
                    data: pointsData,
                    label: this.getJobName(dataSetInput.model.jobs, task.job),
                    borderColor: this.getJobColor(task.job),
                    tooltip: this.generateTooltip(dataSetInput, task.job, task.order)
                });
            }
            machineCount--;
        }

        return {
            data: chartData,
            chartHeight: res.solution.length * 50 + 150,
            options: {
                hover: { mode: null },
                tooltips: {
                    enabled: true,
                    intersect: false,
                    callbacks: {
                        title: (tooltipItem, data) => {
                            return data.datasets[tooltipItem[0].datasetIndex].tooltip[0];
                        },
                        label: (tooltipItem, data) => {
                            return null;
                        },
                        afterLabel: (tooltipItem, data) => {
                            return data.datasets[tooltipItem.datasetIndex].tooltip[1];
                        }
                    }
                },
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: true,
                    onClick: () => null,
                    labels: {
                        filter: (a, b) => {
                            a.lineWidth = 5;
                            if (this.getFirstOcurrence(b.datasets, a.text) === a.datasetIndex) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                },
                scales: {
                    xAxes: [
                        {
                            type: 'linear',
                            position: 'bottom',
                            scaleLabel: {
                                display: true,
                                labelString: labelX
                            },
                            ticks: {
                                beginAtZero: true,
                                stepSize: 1
                            }
                        }
                    ],
                    yAxes: [
                        {
                            scaleLabel: {
                                display: true,
                                labelString: labelY
                            },
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                beginAtZero: true,
                                stepSize: 1,
                                max: res.solution.length,
                                callback: (label, index, labels) => {
                                    if (Number.isInteger(label)) {
                                        return this.getMachineNumber(
                                            dataSetInput.model.machines,
                                            dataSetInput.parsed.machinesReference[dataSetInput.parsed.machinesReference.length - label]
                                        );
                                    } else {
                                        return '';
                                    }
                                }
                            }
                        }
                    ]
                }
            }
        };
    }

    getFirstOcurrence(data, text): number {
        for (const index of Object.keys(data)) {
            if (data[index].label === text) {
                return parseInt(index, 10);
            }
        }
        return -1;
    }

    getJobColor(jobId) {
        if (!this.jobColor[jobId]) {
            const color = this.availableColors.splice(0, 1);
            if (color.length) {
                this.jobColor[jobId] = color[0];
            } else {
                this.jobColor[jobId] = `rgba(${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)}, ${Math.floor(
                    Math.random() * 255
                )}, 1)`;
            }
        }
        return this.jobColor[jobId];
    }

    getJobNames(jobs) {
        const jobNames = [];
        for (const job of jobs) {
            jobNames.push(job.name);
        }
        return jobNames;
    }

    getJobName(jobs, id) {
        for (const job of jobs) {
            if (job.id === parseInt(id, 10)) {
                return job.name;
            }
        }
        return id;
    }

    getMachineNumber(machines, id) {
        for (const machine of machines) {
            if (machine.id === id) {
                return machine.serialNumber;
            }
        }
        return id;
    }

    generateTooltip(data, jobId, order) {
        const jobs = {};
        for (const task of data.model.tasks) {
            if (!jobs[task.jobId]) {
                jobs[task.jobId] = [];
            }
            jobs[task.jobId].push(task);
        }

        for (const job of Object.keys(jobs)) {
            jobs[job] = this.sort.transform(jobs[job], 'order', true);
        }

        const currentTask = jobs[jobId][order];
        return [currentTask.name, currentTask.description];
    }
}
