import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JobShopSharedModule } from 'app/shared';
import {
    ModelRunDefComponent,
    ModelRunDefDetailComponent,
    ModelRunDefUpdateComponent,
    ModelRunDefDeletePopupComponent,
    ModelRunDefDeleteDialogComponent,
    modelRunDefRoute,
    modelRunDefPopupRoute
} from './';

const ENTITY_STATES = [...modelRunDefRoute, ...modelRunDefPopupRoute];

@NgModule({
    imports: [JobShopSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ModelRunDefComponent,
        ModelRunDefDetailComponent,
        ModelRunDefUpdateComponent,
        ModelRunDefDeleteDialogComponent,
        ModelRunDefDeletePopupComponent
    ],
    entryComponents: [ModelRunDefComponent, ModelRunDefUpdateComponent, ModelRunDefDeleteDialogComponent, ModelRunDefDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JobShopModelRunDefModule {}
