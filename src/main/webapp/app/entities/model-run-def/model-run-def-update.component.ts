import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiDataUtils } from 'ng-jhipster';

import { IModelRunDef } from 'app/shared/model/model-run-def.model';
import { ModelRunDefService } from './model-run-def.service';

@Component({
    selector: 'jhi-model-run-def-update',
    templateUrl: './model-run-def-update.component.html'
})
export class ModelRunDefUpdateComponent implements OnInit {
    private _modelRunDef: IModelRunDef;
    isSaving: boolean;
    runDateDp: any;

    constructor(private dataUtils: JhiDataUtils, private modelRunDefService: ModelRunDefService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ modelRunDef }) => {
            this.modelRunDef = modelRunDef;
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.modelRunDef.id !== undefined) {
            this.subscribeToSaveResponse(this.modelRunDefService.update(this.modelRunDef));
        } else {
            this.subscribeToSaveResponse(this.modelRunDefService.create(this.modelRunDef));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IModelRunDef>>) {
        result.subscribe((res: HttpResponse<IModelRunDef>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get modelRunDef() {
        return this._modelRunDef;
    }

    set modelRunDef(modelRunDef: IModelRunDef) {
        this._modelRunDef = modelRunDef;
    }
}
