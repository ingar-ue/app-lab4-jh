import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IModelRunDef } from 'app/shared/model/model-run-def.model';

@Component({
    selector: 'jhi-model-run-def-detail',
    templateUrl: './model-run-def-detail.component.html'
})
export class ModelRunDefDetailComponent implements OnInit {
    modelRunDef: IModelRunDef;

    constructor(private dataUtils: JhiDataUtils, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ modelRunDef }) => {
            this.modelRunDef = modelRunDef;
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }
}
