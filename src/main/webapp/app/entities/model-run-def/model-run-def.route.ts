import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ModelRunDef } from 'app/shared/model/model-run-def.model';
import { ModelRunDefService } from './model-run-def.service';
import { ModelRunDefComponent } from './model-run-def.component';
import { ModelRunDefDetailComponent } from './model-run-def-detail.component';
import { ModelRunDefUpdateComponent } from './model-run-def-update.component';
import { ModelRunDefDeletePopupComponent } from './model-run-def-delete-dialog.component';
import { IModelRunDef } from 'app/shared/model/model-run-def.model';

@Injectable({ providedIn: 'root' })
export class ModelRunDefResolve implements Resolve<IModelRunDef> {
    constructor(private service: ModelRunDefService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((modelRunDef: HttpResponse<ModelRunDef>) => modelRunDef.body));
        }
        return of(new ModelRunDef());
    }
}

export const modelRunDefRoute: Routes = [
    {
        path: 'model-run-def',
        component: ModelRunDefComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jobShopApp.modelRunDef.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'model-run-def/:id/view',
        component: ModelRunDefDetailComponent,
        resolve: {
            modelRunDef: ModelRunDefResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jobShopApp.modelRunDef.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'model-run-def/new',
        component: ModelRunDefUpdateComponent,
        resolve: {
            modelRunDef: ModelRunDefResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jobShopApp.modelRunDef.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'model-run-def/:id/edit',
        component: ModelRunDefUpdateComponent,
        resolve: {
            modelRunDef: ModelRunDefResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jobShopApp.modelRunDef.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const modelRunDefPopupRoute: Routes = [
    {
        path: 'model-run-def/:id/delete',
        component: ModelRunDefDeletePopupComponent,
        resolve: {
            modelRunDef: ModelRunDefResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jobShopApp.modelRunDef.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
