import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { IModelVersion } from 'app/shared/model/model-version.model';
import { ModelVersionService } from './model-version.service';
import { IModelDef } from 'app/shared/model/model-def.model';
import { ModelDefService } from 'app/entities/model-def';

@Component({
    selector: 'jhi-model-version-update',
    templateUrl: './model-version-update.component.html'
})
export class ModelVersionUpdateComponent implements OnInit {
    private _modelVersion: IModelVersion;
    isSaving: boolean;

    modeldefs: IModelDef[];
    creationDateDp: any;

    constructor(
        private dataUtils: JhiDataUtils,
        private jhiAlertService: JhiAlertService,
        private modelVersionService: ModelVersionService,
        private modelDefService: ModelDefService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ modelVersion }) => {
            this.modelVersion = modelVersion;
        });
        this.modelDefService.query().subscribe(
            (res: HttpResponse<IModelDef[]>) => {
                this.modeldefs = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.modelVersion.id !== undefined) {
            this.subscribeToSaveResponse(this.modelVersionService.update(this.modelVersion));
        } else {
            this.subscribeToSaveResponse(this.modelVersionService.create(this.modelVersion));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IModelVersion>>) {
        result.subscribe((res: HttpResponse<IModelVersion>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackModelDefById(index: number, item: IModelDef) {
        return item.id;
    }
    get modelVersion() {
        return this._modelVersion;
    }

    set modelVersion(modelVersion: IModelVersion) {
        this._modelVersion = modelVersion;
    }
}
