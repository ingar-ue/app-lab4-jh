import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ModelVersion } from 'app/shared/model/model-version.model';
import { ModelVersionService } from './model-version.service';
import { ModelVersionComponent } from './model-version.component';
import { ModelVersionDetailComponent } from './model-version-detail.component';
import { ModelVersionUpdateComponent } from './model-version-update.component';
import { ModelVersionDeletePopupComponent } from './model-version-delete-dialog.component';
import { IModelVersion } from 'app/shared/model/model-version.model';

@Injectable({ providedIn: 'root' })
export class ModelVersionResolve implements Resolve<IModelVersion> {
    constructor(private service: ModelVersionService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((modelVersion: HttpResponse<ModelVersion>) => modelVersion.body));
        }
        return of(new ModelVersion());
    }
}

export const modelVersionRoute: Routes = [
    {
        path: 'model-version',
        component: ModelVersionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jobShopApp.modelVersion.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'model-version/:id/view',
        component: ModelVersionDetailComponent,
        resolve: {
            modelVersion: ModelVersionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jobShopApp.modelVersion.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'model-version/new',
        component: ModelVersionUpdateComponent,
        resolve: {
            modelVersion: ModelVersionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jobShopApp.modelVersion.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'model-version/:id/edit',
        component: ModelVersionUpdateComponent,
        resolve: {
            modelVersion: ModelVersionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jobShopApp.modelVersion.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const modelVersionPopupRoute: Routes = [
    {
        path: 'model-version/:id/delete',
        component: ModelVersionDeletePopupComponent,
        resolve: {
            modelVersion: ModelVersionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jobShopApp.modelVersion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
