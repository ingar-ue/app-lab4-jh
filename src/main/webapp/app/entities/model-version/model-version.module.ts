import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JobShopSharedModule } from 'app/shared';
import {
    ModelVersionComponent,
    ModelVersionDetailComponent,
    ModelVersionUpdateComponent,
    ModelVersionDeletePopupComponent,
    ModelVersionDeleteDialogComponent,
    modelVersionRoute,
    modelVersionPopupRoute
} from './';

const ENTITY_STATES = [...modelVersionRoute, ...modelVersionPopupRoute];

@NgModule({
    imports: [JobShopSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ModelVersionComponent,
        ModelVersionDetailComponent,
        ModelVersionUpdateComponent,
        ModelVersionDeleteDialogComponent,
        ModelVersionDeletePopupComponent
    ],
    entryComponents: [
        ModelVersionComponent,
        ModelVersionUpdateComponent,
        ModelVersionDeleteDialogComponent,
        ModelVersionDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JobShopModelVersionModule {}
