import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IModelVersion } from 'app/shared/model/model-version.model';

@Component({
    selector: 'jhi-model-version-detail',
    templateUrl: './model-version-detail.component.html'
})
export class ModelVersionDetailComponent implements OnInit {
    modelVersion: IModelVersion;

    constructor(private dataUtils: JhiDataUtils, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ modelVersion }) => {
            this.modelVersion = modelVersion;
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }
}
