import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IOptimizationPoint1 } from 'app/shared/model/optimization-point-1.model';
import { Principal } from 'app/core';

import { OptimizationPoint1Service } from './optimization-point-1.service';
import { ModelRunDefService } from 'app/entities/model-run-def/model-run-def.service';

import 'ag-grid/dist/styles/ag-grid.css';
import 'ag-grid/dist/styles/ag-theme-balham.css';
import { IModelRunDef, IModelRunDefMessage } from 'app/shared/model/model-run-def.model';
import { IModelObjFunction } from 'app/shared/model/model-obj-function.model';
import { IModelRunConfig } from 'app/shared/model/model-run-config.model';
import { IEnvConfig } from 'app/shared/model/env-config.model';

import { Message } from '@stomp/stompjs';
import { WebSocketService } from 'app/shared';
import { SortPipe } from 'app/shared';
import { Observable, interval } from 'rxjs';

@Component({
    selector: 'jhi-optimization-point-1',
    templateUrl: './optimization-point-1.component.html'
})
export class OptimizationPoint1Component implements OnInit, OnDestroy {
    currentAccount: any;
    demoUser = false;
    eventSubscriber: Subscription;
    optimizationPoint1: IOptimizationPoint1;
    modelRunDef: IModelRunDef;
    modelRunDefChange = 0;
    objFunctions: IModelObjFunction[];
    configs: IModelRunConfig[];
    configValuesLoaded = true;
    messageSubscription: Subscription;
    activeTabId: string;
    alertClosed: boolean;
    alertMessage = '';
    alertType: string;

    /******************* TABLE DATA **************************/
    // 1 - machines
    columnDefs1 = [
        { headerName: 'Id', field: 'id', editable: false },
        { headerName: 'Número serial', field: 'serialNumber', editable: false },
        { headerName: 'Modelo', field: 'model', editable: false }
    ];
    // 2 - jobs
    columnDefs2 = [
        { headerName: 'Id', field: 'id', editable: false },
        { headerName: 'Nombre', field: 'name', editable: false },
        { headerName: 'Descripción', field: 'description', editable: false }
    ];
    // 3 - tasks
    columnDefs3 = [
        { headerName: 'Id', field: 'id', editable: false },
        { headerName: 'Nombre', field: 'name', editable: false },
        { headerName: 'Descripción', field: 'description', editable: false },
        { headerName: 'Tiempo de proceso', field: 'processingTime', editable: true },
        { headerName: 'Orden', field: 'order', editable: true },
        { headerName: 'Id Trabajo', field: 'jobId', editable: true },
        { headerName: 'Id Máquina', field: 'machineId', editable: true }
    ];

    rowData1: any;
    rowData2: any;
    rowData3: any;

    grid1IsValid = false;
    grid2IsValid = false;
    grid3IsValid = false;

    grid1ValidMsg: String;
    grid2ValidMsg: String;
    grid3ValidMsg: String;

    gridApi: any;
    gridColumnApi: any;

    sub: any;

    /************************ CLASS **************************/
    constructor(
        private optimizationPoint1Service: OptimizationPoint1Service,
        private modelRunDefService: ModelRunDefService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private parseLinks: JhiParseLinks,
        private principal: Principal,
        private service: WebSocketService,
        private sort: SortPipe
    ) {
        this.modelRunDef = {};
        this.activeTabId = 'dataTab';
        this.alertClosed = true;
        this.gridApi = {};
    }

    ngOnInit() {
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.principal.hasAuthority('ROLE_DEMO').then(demo => {
            this.demoUser = demo;
        });
        this.loadCase();
        this.registerChangeInOptimizationPoint1s();
        this.subscribeToSocket();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
        this.messageSubscription.unsubscribe();
    }

    registerChangeInOptimizationPoint1s() {
        this.eventSubscriber = this.eventManager.subscribe('optimizationPoint1ListModification', response => this.reset());
    }

    reset() {
        this.loadCase();
        this.registerChangeInOptimizationPoint1s();
        // this.chartInit();
    }

    loadCase() {
        this.optimizationPoint1Service.loadCase().subscribe((res: HttpResponse<IOptimizationPoint1>) => {
            this.optimizationPoint1 = res.body;
            this.optimizationPoint1Service.checkRunning().subscribe((res1: HttpResponse<IModelRunDef>) => {
                if (res1.body) {
                    this.modelRunDef = res1.body;
                    this.modelRunDefChange++;
                    this.activeTabId = 'processTab';
                } else {
                    this.modelRunDef = this.optimizationPoint1.modelRunDef;
                    this.modelRunDefChange++;
                }
            });

            // Populate tables
            this.rowData1 = this.optimizationPoint1.machines;
            this.rowData2 = this.optimizationPoint1.jobs;
            this.rowData3 = this.optimizationPoint1.tasks;

            // Validate data grids and set message
            this.validateCaseData();

            this.optimizationPoint1Service
                .loadObjFunctions(this.optimizationPoint1.modelDefId)
                .subscribe((res1: HttpResponse<IModelObjFunction[]>) => {
                    this.objFunctions = res1.body;
                });
            this.optimizationPoint1Service
                .loadConfigs(this.optimizationPoint1.modelDefId)
                .subscribe((res2: HttpResponse<IModelRunConfig[]>) => {
                    this.configs = res2.body;
                });
        });
    }

    subscribeToSocket() {
        this.messageSubscription = this.service.subscribe().subscribe((message: Message) => {
            const msg: IModelRunDefMessage = JSON.parse(message.body);
            if (msg.name === 'Pattern Generator') {
                this.modelRunDef.status = msg.status;
                this.modelRunDef.progress = msg.progress;
                this.modelRunDef.dataSetResult = msg.result;
                this.modelRunDef.id = msg.id;
                this.modelRunDefChange++;

                if (msg.progress === 100 && msg.status === 'FINISHED') {
                    this.fireSuccessAlert('La ejecución ha finalizado, puede visualizar los resultados en la pestaña correspondiente');
                    this.activeTabId = 'resultsTab';
                }

                if (msg.progress === 100 && msg.status === 'ERROR') {
                    this.modelRunDef.comments = msg.result;
                    this.activeTabId = 'resultsTab';
                }
            }
        });
    }

    run() {
        let url = '';
        this.modelRunDef.progress = 5;
        this.sub = interval(5000).subscribe(val => {
            if (this.modelRunDef.progress < 95) {
                this.modelRunDef.progress += 5;
            }
        });
        this.optimizationPoint1Service.getSolverServerURL().subscribe(
            (res1: HttpResponse<IEnvConfig>) => {
                url = res1.body.value;

                this.optimizationPoint1Service.executeOptimization(url, this.parseSendData()).subscribe(
                    (res: JSON) => {
                        this.sub.unsubscribe();
                        this.modelRunDef.progress = 100;
                        this.modelRunDef.status = 'FINISHED';
                        this.modelRunDef.userResult = this.parseResult(res);

                        this.modelRunDefService.create(this.modelRunDef).subscribe();
                        this.fireSuccessAlert('La ejecución ha finalizado, puede visualizar los resultados en la pestaña correspondiente');
                        this.activeTabId = 'resultsTab';
                    },
                    err => this.catchError(err)
                );
            },
            err => this.catchError(err)
        );
    }

    catchError(err) {
        this.sub.unsubscribe();
        this.modelRunDef.progress = 100;
        this.modelRunDef.status = 'ERROR';
        this.fireErrorAlert(err.message);
    }

    parseSendData() {
        const data = { jobsReference: [], machinesReference: [], jobs: [] };
        const jobs = {};

        for (const index of Object.keys(this.optimizationPoint1.tasks)) {
            const task = this.optimizationPoint1.tasks[index];
            // chequear orden y a que trabajo pertenece la tarea
            if (!jobs[task.jobId]) {
                jobs[task.jobId] = [];
            }
            jobs[task.jobId].push(task);
        }

        for (const job of Object.keys(jobs)) {
            // ordenar cada tarea y colocar tiempo
            data.jobs.push(
                this.sort.transform(jobs[job], 'order', true).map(item => {
                    if (data.machinesReference.indexOf(item.machineId) === -1) {
                        data.machinesReference.push(item.machineId);
                    }
                    return [data.machinesReference.indexOf(item.machineId), item.processingTime];
                })
            );
            data.jobsReference.push(job);
        }

        // guardar data set (persiste en db)
        this.modelRunDef.otherResult = btoa(
            JSON.stringify({
                model: {
                    jobs: this.optimizationPoint1.jobs,
                    machines: this.optimizationPoint1.machines,
                    tasks: this.optimizationPoint1.tasks
                },
                parsed: data
            })
        );

        return data;
    }

    parseResult(res) {
        const data = { time: res.time, solution: [] };
        const dataSetInput = JSON.parse(atob(this.modelRunDef.otherResult));

        for (const index of Object.keys(res.solution)) {
            const machine = res.solution[index];
            data.solution.push(
                machine.map(item => {
                    return {
                        job: dataSetInput.parsed.jobsReference[item[2]],
                        machine: dataSetInput.parsed.machinesReference[index],
                        start: item[0],
                        duration: item[1],
                        order: item[3]
                    };
                })
            );
        }

        return btoa(JSON.stringify(data));
    }

    fireSuccessAlert(msg: string) {
        this.alertMessage = msg;
        this.alertClosed = false;
        this.alertType = 'success';
        setTimeout(() => (this.alertClosed = true), 10000);
    }

    fireErrorAlert(msg: string) {
        this.alertMessage = msg;
        this.alertClosed = false;
        this.alertType = 'danger';
        setTimeout(() => (this.alertClosed = true), 10000);
    }

    configChange() {
        this.configValuesLoaded = true;
        for (const config of this.configs) {
            if (!config.value) {
                this.configValuesLoaded = false;
            }
        }
    }

    tabChange(e) {
        this.activeTabId = e.nextId;
    }

    /******************* TABLE FUNCTIONS **************************/
    validateCaseData() {
        // grid 1
        if (this.rowData1.length > 0) {
            this.grid1IsValid = true;
            this.grid1ValidMsg = 'Datos correctos';
        } else {
            this.grid1IsValid = false;
            this.grid1ValidMsg = 'Error de validación: Debe haber al menos un registro cargado';
        }
        // grid 2
        if (this.rowData2.length > 0) {
            this.grid2IsValid = true;
            this.grid2ValidMsg = 'Datos correctos';
        } else {
            this.grid2IsValid = false;
            this.grid2ValidMsg = 'Error de validación: Debe haber al menos un registro cargado';
        }
        // grid 3
        if (this.rowData3.length > 0) {
            this.grid3IsValid = true;
            this.grid3ValidMsg = 'Datos correctos';
        } else {
            this.grid3IsValid = false;
            this.grid3ValidMsg = 'Error de validación: Debe haber al menos un registro cargado';
        }
    }

    onGridReady(params, id) {
        this.gridApi[id] = params.api;
        this.gridColumnApi = params.columnApi;
        // Setea ancho de las columnas para llenar ancho de la tabla
        params.api.sizeColumnsToFit();
        // Si la cantidad de filas a mostrar es mayor que 25 recorta el largo de la tabla
        console.log('params.api.getDisplayedRowCount() ' + params.api.getDisplayedRowCount());
        if (params.api.getDisplayedRowCount() > 10) {
            params.api.setGridAutoHeight(false);
            const theGrid: HTMLElement = <HTMLElement>document.querySelector('#' + id);
            theGrid.style.height = '300px';
        } else {
            params.api.setGridAutoHeight(true);
            const theGrid: HTMLElement = <HTMLElement>document.querySelector('#' + id);
            theGrid.style.height = '';
        }
    }
}
