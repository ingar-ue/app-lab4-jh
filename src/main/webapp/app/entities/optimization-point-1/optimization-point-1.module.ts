import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JobShopSharedModule } from 'app/shared';
import {
    OptimizationPoint1Component,
    OptimizationPoint1ExecutePopupComponent,
    OptimizationPoint1ExecuteDialogComponent,
    optimizationPoint1Route,
    optimizationPoint1PopupRoute
} from './';

import { AgGridModule } from 'ag-grid-angular';

import 'ag-grid/dist/styles/ag-grid.css';
import 'ag-grid/dist/styles/ag-theme-balham.css';

import { ChartModule } from 'angular2-chartjs';

const ENTITY_STATES = [...optimizationPoint1Route, ...optimizationPoint1PopupRoute];

@NgModule({
    imports: [JobShopSharedModule, RouterModule.forChild(ENTITY_STATES), AgGridModule.withComponents([]), ChartModule],
    declarations: [OptimizationPoint1Component, OptimizationPoint1ExecutePopupComponent, OptimizationPoint1ExecuteDialogComponent],
    entryComponents: [OptimizationPoint1Component, OptimizationPoint1ExecutePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JobShopOptimizationPoint1Module {}
