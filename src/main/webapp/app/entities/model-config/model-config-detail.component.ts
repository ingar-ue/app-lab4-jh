import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IModelConfig } from 'app/shared/model/model-config.model';

@Component({
    selector: 'jhi-model-config-detail',
    templateUrl: './model-config-detail.component.html'
})
export class ModelConfigDetailComponent implements OnInit {
    modelConfig: IModelConfig;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ modelConfig }) => {
            this.modelConfig = modelConfig;
        });
    }

    previousState() {
        window.history.back();
    }
}
