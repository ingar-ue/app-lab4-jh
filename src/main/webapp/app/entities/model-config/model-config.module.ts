import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JobShopSharedModule } from 'app/shared';
import {
    ModelConfigComponent,
    ModelConfigDetailComponent,
    ModelConfigUpdateComponent,
    ModelConfigDeletePopupComponent,
    ModelConfigDeleteDialogComponent,
    modelConfigRoute,
    modelConfigPopupRoute
} from './';

const ENTITY_STATES = [...modelConfigRoute, ...modelConfigPopupRoute];

@NgModule({
    imports: [JobShopSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ModelConfigComponent,
        ModelConfigDetailComponent,
        ModelConfigUpdateComponent,
        ModelConfigDeleteDialogComponent,
        ModelConfigDeletePopupComponent
    ],
    entryComponents: [ModelConfigComponent, ModelConfigUpdateComponent, ModelConfigDeleteDialogComponent, ModelConfigDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JobShopModelConfigModule {}
