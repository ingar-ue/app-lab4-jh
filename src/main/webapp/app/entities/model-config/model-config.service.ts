import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IModelConfig } from 'app/shared/model/model-config.model';

type EntityResponseType = HttpResponse<IModelConfig>;
type EntityArrayResponseType = HttpResponse<IModelConfig[]>;

@Injectable({ providedIn: 'root' })
export class ModelConfigService {
    private resourceUrl = SERVER_API_URL + 'api/model-configs';

    constructor(private http: HttpClient) {}

    create(modelConfig: IModelConfig): Observable<EntityResponseType> {
        return this.http.post<IModelConfig>(this.resourceUrl, modelConfig, { observe: 'response' });
    }

    update(modelConfig: IModelConfig): Observable<EntityResponseType> {
        return this.http.put<IModelConfig>(this.resourceUrl, modelConfig, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IModelConfig>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IModelConfig[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
