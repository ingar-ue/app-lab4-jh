import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IModelConfig } from 'app/shared/model/model-config.model';
import { ModelConfigService } from './model-config.service';
import { IModelDef } from 'app/shared/model/model-def.model';
import { ModelDefService } from 'app/entities/model-def';

@Component({
    selector: 'jhi-model-config-update',
    templateUrl: './model-config-update.component.html'
})
export class ModelConfigUpdateComponent implements OnInit {
    private _modelConfig: IModelConfig;
    isSaving: boolean;

    modeldefs: IModelDef[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private modelConfigService: ModelConfigService,
        private modelDefService: ModelDefService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ modelConfig }) => {
            this.modelConfig = modelConfig;
        });
        this.modelDefService.query().subscribe(
            (res: HttpResponse<IModelDef[]>) => {
                this.modeldefs = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.modelConfig.id !== undefined) {
            this.subscribeToSaveResponse(this.modelConfigService.update(this.modelConfig));
        } else {
            this.subscribeToSaveResponse(this.modelConfigService.create(this.modelConfig));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IModelConfig>>) {
        result.subscribe((res: HttpResponse<IModelConfig>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackModelDefById(index: number, item: IModelDef) {
        return item.id;
    }
    get modelConfig() {
        return this._modelConfig;
    }

    set modelConfig(modelConfig: IModelConfig) {
        this._modelConfig = modelConfig;
    }
}
