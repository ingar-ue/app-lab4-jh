export * from './model-obj-function.service';
export * from './model-obj-function-update.component';
export * from './model-obj-function-delete-dialog.component';
export * from './model-obj-function-detail.component';
export * from './model-obj-function.component';
export * from './model-obj-function.route';
