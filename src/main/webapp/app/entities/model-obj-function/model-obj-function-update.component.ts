import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IModelObjFunction } from 'app/shared/model/model-obj-function.model';
import { ModelObjFunctionService } from './model-obj-function.service';
import { IModelDef } from 'app/shared/model/model-def.model';
import { ModelDefService } from 'app/entities/model-def';

@Component({
    selector: 'jhi-model-obj-function-update',
    templateUrl: './model-obj-function-update.component.html'
})
export class ModelObjFunctionUpdateComponent implements OnInit {
    private _modelObjFunction: IModelObjFunction;
    isSaving: boolean;

    modeldefs: IModelDef[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private modelObjFunctionService: ModelObjFunctionService,
        private modelDefService: ModelDefService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ modelObjFunction }) => {
            this.modelObjFunction = modelObjFunction;
        });
        this.modelDefService.query().subscribe(
            (res: HttpResponse<IModelDef[]>) => {
                this.modeldefs = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.modelObjFunction.id !== undefined) {
            this.subscribeToSaveResponse(this.modelObjFunctionService.update(this.modelObjFunction));
        } else {
            this.subscribeToSaveResponse(this.modelObjFunctionService.create(this.modelObjFunction));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IModelObjFunction>>) {
        result.subscribe((res: HttpResponse<IModelObjFunction>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackModelDefById(index: number, item: IModelDef) {
        return item.id;
    }
    get modelObjFunction() {
        return this._modelObjFunction;
    }

    set modelObjFunction(modelObjFunction: IModelObjFunction) {
        this._modelObjFunction = modelObjFunction;
    }
}
