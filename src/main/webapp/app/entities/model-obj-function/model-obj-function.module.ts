import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JobShopSharedModule } from 'app/shared';
import {
    ModelObjFunctionComponent,
    ModelObjFunctionDetailComponent,
    ModelObjFunctionUpdateComponent,
    ModelObjFunctionDeletePopupComponent,
    ModelObjFunctionDeleteDialogComponent,
    modelObjFunctionRoute,
    modelObjFunctionPopupRoute
} from './';

const ENTITY_STATES = [...modelObjFunctionRoute, ...modelObjFunctionPopupRoute];

@NgModule({
    imports: [JobShopSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ModelObjFunctionComponent,
        ModelObjFunctionDetailComponent,
        ModelObjFunctionUpdateComponent,
        ModelObjFunctionDeleteDialogComponent,
        ModelObjFunctionDeletePopupComponent
    ],
    entryComponents: [
        ModelObjFunctionComponent,
        ModelObjFunctionUpdateComponent,
        ModelObjFunctionDeleteDialogComponent,
        ModelObjFunctionDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JobShopModelObjFunctionModule {}
