import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IModelObjFunction } from 'app/shared/model/model-obj-function.model';

@Component({
    selector: 'jhi-model-obj-function-detail',
    templateUrl: './model-obj-function-detail.component.html'
})
export class ModelObjFunctionDetailComponent implements OnInit {
    modelObjFunction: IModelObjFunction;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ modelObjFunction }) => {
            this.modelObjFunction = modelObjFunction;
        });
    }

    previousState() {
        window.history.back();
    }
}
