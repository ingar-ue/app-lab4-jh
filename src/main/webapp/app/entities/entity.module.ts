import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { JobShopJobModule } from './job/job.module';
import { JobShopTaskModule } from './task/task.module';
import { JobShopMachineModule } from './machine/machine.module';
import { JobShopEnvConfigModule } from './env-config/env-config.module';
import { JobShopModelDefModule } from './model-def/model-def.module';
import { JobShopModelConfigModule } from './model-config/model-config.module';
import { JobShopModelVersionModule } from './model-version/model-version.module';
import { JobShopModelObjFunctionModule } from './model-obj-function/model-obj-function.module';
import { JobShopModelRunDefModule } from './model-run-def/model-run-def.module';
import { JobShopModelRunConfigModule } from './model-run-config/model-run-config.module';
import { JobShopOptimizationPoint1Module } from './optimization-point-1/optimization-point-1.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        JobShopJobModule,
        JobShopTaskModule,
        JobShopMachineModule,
        JobShopEnvConfigModule,
        JobShopModelDefModule,
        JobShopModelConfigModule,
        JobShopModelVersionModule,
        JobShopModelObjFunctionModule,
        JobShopModelRunDefModule,
        JobShopModelRunConfigModule,
        JobShopOptimizationPoint1Module,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JobShopEntityModule {}
