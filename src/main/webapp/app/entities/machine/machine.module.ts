import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JobShopSharedModule } from 'app/shared';
import {
    MachineComponent,
    MachineDetailComponent,
    MachineUpdateComponent,
    MachineDeletePopupComponent,
    MachineDeleteDialogComponent,
    machineRoute,
    machinePopupRoute
} from './';

const ENTITY_STATES = [...machineRoute, ...machinePopupRoute];

@NgModule({
    imports: [JobShopSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        MachineComponent,
        MachineDetailComponent,
        MachineUpdateComponent,
        MachineDeleteDialogComponent,
        MachineDeletePopupComponent
    ],
    entryComponents: [MachineComponent, MachineUpdateComponent, MachineDeleteDialogComponent, MachineDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JobShopMachineModule {}
