import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IMachine } from 'app/shared/model/machine.model';
import { MachineService } from './machine.service';

@Component({
    selector: 'jhi-machine-update',
    templateUrl: './machine-update.component.html'
})
export class MachineUpdateComponent implements OnInit {
    private _machine: IMachine;
    isSaving: boolean;

    constructor(private machineService: MachineService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ machine }) => {
            this.machine = machine;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.machine.id !== undefined) {
            this.subscribeToSaveResponse(this.machineService.update(this.machine));
        } else {
            this.subscribeToSaveResponse(this.machineService.create(this.machine));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IMachine>>) {
        result.subscribe((res: HttpResponse<IMachine>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get machine() {
        return this._machine;
    }

    set machine(machine: IMachine) {
        this._machine = machine;
    }
}
