import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Machine } from 'app/shared/model/machine.model';
import { MachineService } from './machine.service';
import { MachineComponent } from './machine.component';
import { MachineDetailComponent } from './machine-detail.component';
import { MachineUpdateComponent } from './machine-update.component';
import { MachineDeletePopupComponent } from './machine-delete-dialog.component';
import { IMachine } from 'app/shared/model/machine.model';

@Injectable({ providedIn: 'root' })
export class MachineResolve implements Resolve<IMachine> {
    constructor(private service: MachineService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((machine: HttpResponse<Machine>) => machine.body));
        }
        return of(new Machine());
    }
}

export const machineRoute: Routes = [
    {
        path: 'machine',
        component: MachineComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jobShopApp.machine.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'machine/:id/view',
        component: MachineDetailComponent,
        resolve: {
            machine: MachineResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jobShopApp.machine.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'machine/new',
        component: MachineUpdateComponent,
        resolve: {
            machine: MachineResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jobShopApp.machine.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'machine/:id/edit',
        component: MachineUpdateComponent,
        resolve: {
            machine: MachineResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jobShopApp.machine.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const machinePopupRoute: Routes = [
    {
        path: 'machine/:id/delete',
        component: MachineDeletePopupComponent,
        resolve: {
            machine: MachineResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jobShopApp.machine.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
