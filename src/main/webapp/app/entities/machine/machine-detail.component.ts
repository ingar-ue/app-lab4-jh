import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMachine } from 'app/shared/model/machine.model';

@Component({
    selector: 'jhi-machine-detail',
    templateUrl: './machine-detail.component.html'
})
export class MachineDetailComponent implements OnInit {
    machine: IMachine;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ machine }) => {
            this.machine = machine;
        });
    }

    previousState() {
        window.history.back();
    }
}
