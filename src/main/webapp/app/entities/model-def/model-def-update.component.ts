import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IModelDef } from 'app/shared/model/model-def.model';
import { ModelDefService } from './model-def.service';

@Component({
    selector: 'jhi-model-def-update',
    templateUrl: './model-def-update.component.html'
})
export class ModelDefUpdateComponent implements OnInit {
    private _modelDef: IModelDef;
    isSaving: boolean;

    constructor(private modelDefService: ModelDefService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ modelDef }) => {
            this.modelDef = modelDef;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.modelDef.id !== undefined) {
            this.subscribeToSaveResponse(this.modelDefService.update(this.modelDef));
        } else {
            this.subscribeToSaveResponse(this.modelDefService.create(this.modelDef));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IModelDef>>) {
        result.subscribe((res: HttpResponse<IModelDef>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get modelDef() {
        return this._modelDef;
    }

    set modelDef(modelDef: IModelDef) {
        this._modelDef = modelDef;
    }
}
