import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JobShopSharedModule } from 'app/shared';
import {
    ModelDefComponent,
    ModelDefDetailComponent,
    ModelDefUpdateComponent,
    ModelDefDeletePopupComponent,
    ModelDefDeleteDialogComponent,
    modelDefRoute,
    modelDefPopupRoute
} from './';

const ENTITY_STATES = [...modelDefRoute, ...modelDefPopupRoute];

@NgModule({
    imports: [JobShopSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ModelDefComponent,
        ModelDefDetailComponent,
        ModelDefUpdateComponent,
        ModelDefDeleteDialogComponent,
        ModelDefDeletePopupComponent
    ],
    entryComponents: [ModelDefComponent, ModelDefUpdateComponent, ModelDefDeleteDialogComponent, ModelDefDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JobShopModelDefModule {}
