import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IModelDef } from 'app/shared/model/model-def.model';

@Component({
    selector: 'jhi-model-def-detail',
    templateUrl: './model-def-detail.component.html'
})
export class ModelDefDetailComponent implements OnInit {
    modelDef: IModelDef;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ modelDef }) => {
            this.modelDef = modelDef;
        });
    }

    previousState() {
        window.history.back();
    }
}
