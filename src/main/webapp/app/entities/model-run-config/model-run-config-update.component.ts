import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IModelRunConfig } from 'app/shared/model/model-run-config.model';
import { ModelRunConfigService } from './model-run-config.service';
import { IModelRunDef } from 'app/shared/model/model-run-def.model';
import { ModelRunDefService } from 'app/entities/model-run-def';

@Component({
    selector: 'jhi-model-run-config-update',
    templateUrl: './model-run-config-update.component.html'
})
export class ModelRunConfigUpdateComponent implements OnInit {
    private _modelRunConfig: IModelRunConfig;
    isSaving: boolean;

    modelrundefs: IModelRunDef[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private modelRunConfigService: ModelRunConfigService,
        private modelRunDefService: ModelRunDefService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ modelRunConfig }) => {
            this.modelRunConfig = modelRunConfig;
        });
        this.modelRunDefService.query().subscribe(
            (res: HttpResponse<IModelRunDef[]>) => {
                this.modelrundefs = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.modelRunConfig.id !== undefined) {
            this.subscribeToSaveResponse(this.modelRunConfigService.update(this.modelRunConfig));
        } else {
            this.subscribeToSaveResponse(this.modelRunConfigService.create(this.modelRunConfig));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IModelRunConfig>>) {
        result.subscribe((res: HttpResponse<IModelRunConfig>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackModelRunDefById(index: number, item: IModelRunDef) {
        return item.id;
    }
    get modelRunConfig() {
        return this._modelRunConfig;
    }

    set modelRunConfig(modelRunConfig: IModelRunConfig) {
        this._modelRunConfig = modelRunConfig;
    }
}
