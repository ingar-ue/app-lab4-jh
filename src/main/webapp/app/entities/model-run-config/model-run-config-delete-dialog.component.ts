import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IModelRunConfig } from 'app/shared/model/model-run-config.model';
import { ModelRunConfigService } from './model-run-config.service';

@Component({
    selector: 'jhi-model-run-config-delete-dialog',
    templateUrl: './model-run-config-delete-dialog.component.html'
})
export class ModelRunConfigDeleteDialogComponent {
    modelRunConfig: IModelRunConfig;

    constructor(
        private modelRunConfigService: ModelRunConfigService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.modelRunConfigService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'modelRunConfigListModification',
                content: 'Deleted an modelRunConfig'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-model-run-config-delete-popup',
    template: ''
})
export class ModelRunConfigDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ modelRunConfig }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ModelRunConfigDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.modelRunConfig = modelRunConfig;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
