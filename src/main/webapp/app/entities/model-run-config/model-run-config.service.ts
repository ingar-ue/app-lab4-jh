import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IModelRunConfig } from 'app/shared/model/model-run-config.model';

type EntityResponseType = HttpResponse<IModelRunConfig>;
type EntityArrayResponseType = HttpResponse<IModelRunConfig[]>;

@Injectable({ providedIn: 'root' })
export class ModelRunConfigService {
    private resourceUrl = SERVER_API_URL + 'api/model-run-configs';

    constructor(private http: HttpClient) {}

    create(modelRunConfig: IModelRunConfig): Observable<EntityResponseType> {
        return this.http.post<IModelRunConfig>(this.resourceUrl, modelRunConfig, { observe: 'response' });
    }

    update(modelRunConfig: IModelRunConfig): Observable<EntityResponseType> {
        return this.http.put<IModelRunConfig>(this.resourceUrl, modelRunConfig, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IModelRunConfig>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IModelRunConfig[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
