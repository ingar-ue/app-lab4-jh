# JobShop
This application was generated using JHipster 5.3.0, you can find documentation and help at [https://www.jhipster.tech/documentation-archive/v5.3.0](https://www.jhipster.tech/documentation-archive/v5.3.0).

## Development

Before you can build this project, you must install and configure the following dependencies on your machine:

1. [Node.js][]: We use Node to run a development web server and build the project.
   Depending on your system, you can install Node either from source or as a pre-packaged bundle.

After installing Node, you should be able to run the following command to install development tools.
You will only need to run this command when dependencies change in [package.json](package.json).

    npm install

We use npm scripts and [Webpack][] as our build system.

Run the following commands in two separate terminals to create a blissful development experience where your browser
auto-refreshes when files change on your hard drive.

    ./mvnw
    npm start

[Npm][] is also used to manage CSS and JavaScript dependencies used in this application. You can upgrade dependencies by
specifying a newer version in [package.json](package.json). You can also run `npm update` and `npm install` to manage dependencies.
Add the `help` flag on any command to see how you can use it. For example, `npm help update`.

The `npm run` command will list all of the scripts available to run for this project.

### Service workers

Service workers are commented by default, to enable them please uncomment the following code.

* The service worker registering script in index.html

```html
<script>
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker
        .register('./service-worker.js')
        .then(function() { console.log('Service Worker Registered'); });
    }
</script>
```

Note: workbox creates the respective service worker and dynamically generate the `service-worker.js`

### Managing dependencies

For example, to add [Leaflet][] library as a runtime dependency of your application, you would run following command:

    npm install --save --save-exact leaflet

To benefit from TypeScript type definitions from [DefinitelyTyped][] repository in development, you would run following command:

    npm install --save-dev --save-exact @types/leaflet

Then you would import the JS and CSS files specified in library's installation instructions so that [Webpack][] knows about them:
Edit [src/main/webapp/app/vendor.ts](src/main/webapp/app/vendor.ts) file:
~~~
import 'leaflet/dist/leaflet.js';
~~~

Edit [src/main/webapp/content/css/vendor.css](src/main/webapp/content/css/vendor.css) file:
~~~
@import '~leaflet/dist/leaflet.css';
~~~
Note: there are still few other things remaining to do for Leaflet that we won't detail here.

For further instructions on how to develop with JHipster, have a look at [Using JHipster in development][].

### Using angular-cli

You can also use [Angular CLI][] to generate some custom client code.

For example, the following command:

    ng generate component my-component

will generate few files:

    create src/main/webapp/app/my-component/my-component.component.html
    create src/main/webapp/app/my-component/my-component.component.ts
    update src/main/webapp/app/app.module.ts


## Building for production

To optimize the JobShop application for production, run:

    ./mvnw -Pprod clean package

This will concatenate and minify the client CSS and JavaScript files. It will also modify `index.html` so it references these new files.
To ensure everything worked, run:

    java -jar target/*.war

Then navigate to [http://localhost:8080](http://localhost:8080) in your browser.

Refer to [Using JHipster in production][] for more details.

## Testing

To launch your application's tests, run:

    ./mvnw clean test

### Client tests

Unit tests are run by [Jest][] and written with [Jasmine][]. They're located in [src/test/javascript/](src/test/javascript/) and can be run with:

    npm test



For more information, refer to the [Running tests page][].

### Code quality

Sonar is used to analyse code quality. You can start a local Sonar server (accessible on http://localhost:9001) with:

```
docker-compose -f src/main/docker/sonar.yml up -d
```

Then, run a Sonar analysis:

```
./mvnw -Pprod clean test sonar:sonar
```

For more information, refer to the [Code quality page][].

## Using Docker to simplify development (optional)

You can use Docker to improve your JHipster development experience. A number of docker-compose configuration are available in the [src/main/docker](src/main/docker) folder to launch required third party services.

For example, to start a postgresql database in a docker container, run:

    docker-compose -f src/main/docker/postgresql.yml up -d

To stop it and remove the container, run:

    docker-compose -f src/main/docker/postgresql.yml down

You can also fully dockerize your application and all the services that it depends on.
To achieve this, first build a docker image of your app by running:

    ./mvnw verify -Pprod dockerfile:build dockerfile:tag@version dockerfile:tag@commit

Then run:

    docker-compose -f src/main/docker/app.yml up -d

For more information refer to [Using Docker and Docker-Compose][], this page also contains information on the docker-compose sub-generator (`jhipster docker-compose`), which is able to generate docker configurations for one or several JHipster applications.

## Build & Deploy at INGAR (Windows)

- Open command prompt as admin an connect to the server by [OPENSSH](http://www.mls-software.com/opensshd.html)
    - ```ssh ingarue@172.16.249.160 -p 22```
    - password: ```avellaneda3657```
- In case of the first deploy (the Postgres database must be created):
    - ```cd c:\code\```
    - git clone https://gitlab.com/ingar-ue/app-lab4-jh-.git 
    (check user permissions, in another case, use https://\<username\>:\<password\>@gitlab.com/ingar-ue/app-lab4-jh-.git)
    - ```npm install``` 
- In case of an update:
    - ```cd c:\code\app-lab4-jh```
    - ```git pull origin master```
    - ```npm install``` (for new dependencies)
- ```mvn clean -Pprod -DskipTests package``` (production profile)
- ```net stop ueapp4``` (stop service ueapp4)
- ```del C:\apps\app-lab4-jh\app-lab4.log```
- ```copy c:\code\app-lab4-jh\target\job-shop-0.0.1.war C:\apps\app-lab4-jh```
- ```net start ueapp4``` (start service ueapp4)

### View live logs en powershell (Windows)

- ```Get-Content -Path "C:\apps\app-lab4-jh\app-lab4.log" -Wait```

## Build & Deploy at INGARUEX (Linux Ubuntu)

- Open command prompt as admin an connect to the server by [OPENSSH](http://www.mls-software.com/opensshd.html)
    - ```ssh ingarue@172.16.249.160 -p 22```
    - password: ```avellaneda3657```
- In case of the first deploy (the Postgres database must be created):
    - ```cd /home/ingarue/code/```
    - git clone https://gitlab.com/ingar-ue/app-lab4-jh.git 
    (check user permissions, in another case, use https://\<username\>:\<password\>@gitlab.com/ingar-ue/app-lab4-jh.git)
- In case of an update:
    - ```cd /home/ingarue/code/app-lab4-jh```
    - ```git pull origin master```
- ```mvn clean -Pprod -DskipTests package``` (production profile)

- ```net stop ueapp4``` (stop service ueapp4)
- ```del C:\apps\app-lab4-jh\app-lab4.log```
- ```sudo cp /home/ingarue/code/app-lab4-jh/target/app-lab4.war /opt/app-lab4-jh```
- ```net start ueapp4``` (start service ueapp4)

### View live logs en powershell (Windows)

- ```Get-Content -Path "C:\apps\app-lab4-jh\app-lab4.log" -Wait```

### Create linux service

sudo nano /etc/systemd/system/app-lab4.service

[Unit]
Description=App Lab 4 Core Service
[Service]
WorkingDirectory=/opt/app-lab4-jh
#path to executable. 
ExecStart=/opt/app-lab4-jh/app-lab4-jh.sh
SuccessExitStatus=143
TimeoutStopSec=10
Restart=on-failure
RestartSec=5
[Install]
WantedBy=multi-user.target
